#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/rectangle.h"

TEST(RectangleTets, LegalTest){
    // 向量A的x與向量B的x相同
    ASSERT_NO_THROW(Rectangle rec1(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0))));
    // 向量A的x與向量B的y相同
    ASSERT_NO_THROW(Rectangle rec2(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(1, 0), new Point(0, 0))));
    // 向量A的y與向量B的x相同
    ASSERT_NO_THROW(Rectangle rec3(new TwoDimensionalVector(new Point(0, 1), new Point(0, 0)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0))));
    // 向量A的y與向量B的y相同
    ASSERT_NO_THROW(Rectangle rec4(new TwoDimensionalVector(new Point(0, 1), new Point(0, 0)), new TwoDimensionalVector(new Point(1, 0), new Point(0, 0))));
}

TEST(RectangleTest, IllegalTest){
    // 兩向量無任一點重疊
    ASSERT_ANY_THROW(Rectangle rec1(new TwoDimensionalVector(new Point(1, 1), new Point(2, 2)), new TwoDimensionalVector(new Point(0, 0), new Point(0, 1))));
    // 兩向量為同一向量
    ASSERT_ANY_THROW(Rectangle rec2(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(0, 1))));
    // 兩向量平行
    ASSERT_ANY_THROW(Rectangle rec3(new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)), new TwoDimensionalVector(new Point(0, 1), new Point(1, 1))));
    // 兩向量正交但無任一點重疊
    ASSERT_ANY_THROW(Rectangle rec4(new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)), new TwoDimensionalVector(new Point(0, 1), new Point(0, 2))));

}

TEST(RectangleTest, LengthTest){
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    ASSERT_NEAR(1, rec.length(), 0.001);
}

TEST(RectangleTest, WidthTest){
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    ASSERT_NEAR(1, rec.width(), 0.001);
}

TEST(RectangleTest, AreaTest){
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    ASSERT_NEAR(1, rec.area(), 0.001);
}

TEST(RectangleTest, PerimeterTest){
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    ASSERT_NEAR(4, rec.perimeter(), 0.001);
}

TEST(RectangleTest, InfoTest){
    Rectangle rec(new TwoDimensionalVector(new Point(-2, 1), new Point(1.5, 0.47)), new TwoDimensionalVector(new Point(-2, 1), new Point(-1.47, 4.5)));
    ASSERT_EQ("Rectangle (Vector ((-2.00, 1.00), (1.50, 0.47)), Vector ((-2.00, 1.00), (-1.47, 4.50)))", rec.info());
}