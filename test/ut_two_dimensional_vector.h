#include "../src/point.h"
#include "../src/two_dimensional_vector.h"

TEST(TwoDimensionalVectorTest, APointTest){
    TwoDimensionalVector v(new Point(-8.42, 3.42), new Point(-3.38, 4.3));
    Point a(-8.42, 3.42);
    ASSERT_EQ(a, *(v.a()));
}

TEST(TwoDimensionalVectorTest, BPointTest){
    TwoDimensionalVector v(new Point(-8.42, 3.42), new Point(-3.38, 4.3));
    Point b(-3.38, 4.3);
    ASSERT_EQ(b, *(v.b()));
}

TEST(TwoDimensionalVectorTest, LengthTest){
    TwoDimensionalVector v(new Point(10, 5), new Point(5, 8));
    ASSERT_NEAR(5.83, v.length(), 0.001);
}

TEST(TwoDimensionalVectorTest, DotTest){
    TwoDimensionalVector v1(new Point(3, 4), new Point(5, 6));
    TwoDimensionalVector v2(new Point(1, 2), new Point(10, 8));
    ASSERT_NEAR(30, v1.dot(&v2), 0.001);
}

TEST(TwoDimensionalVectorTest, CrossTest){
    TwoDimensionalVector v1(new Point(3, 4), new Point(5, 6));
    TwoDimensionalVector v2(new Point(1, 2), new Point(10, 8));
    ASSERT_NEAR(-6, v1.cross(&v2), 0.001);
}

TEST(TwoDimensionalVectorTest, InfoTest){
    TwoDimensionalVector v(new Point(-8.42, 3.42), new Point(-3, 4.3));
    ASSERT_EQ("Vector ((-8.42, 3.42), (-3.00, 4.30))", v.info());
}