#include "../src/point.h"


TEST(PointTest, XPointTest){
    Point p(1.13544, 2);
    ASSERT_NEAR(1.13544, p.x(), 0.001);
}

TEST(PointTest, YPointTest){
    Point p(1, 2);
    ASSERT_NEAR(2.00, p.y(), 0.001);
}

TEST(PointTest, OperatorPointTest){
    Point p1(3, 0);
    Point p2(3, 0);
    Point p3(2, 1);
    ASSERT_TRUE(p1==p2);
    ASSERT_FALSE(p1==p3);
}

TEST(PointTest, InfoTest){
    Point p1(-4.586, -3.471);
    ASSERT_EQ("(-4.59, -3.47)", p1.info());
}