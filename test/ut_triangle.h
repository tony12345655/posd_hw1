#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/triangle.h"

TEST(TriangleTst, LegalTest){
    // 向量A的x與向量B的x相同
    ASSERT_NO_THROW(Triangle tri1(new TwoDimensionalVector(new Point(3, 0), new Point(1, 0)), new TwoDimensionalVector(new Point(3, 0), new Point(7, 1))));
    // 向量A的x與向量B的y相同
    ASSERT_NO_THROW(Triangle tri2(new TwoDimensionalVector(new Point(3, 0), new Point(1, 0)), new TwoDimensionalVector(new Point(7, 1), new Point(3, 0))));
    // 向量A的y與向量B的x相同
    ASSERT_NO_THROW(Triangle tri3(new TwoDimensionalVector(new Point(1, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 0), new Point(7, 1))));
    // 向量A的y與向量B的y相同
    ASSERT_NO_THROW(Triangle tri4(new TwoDimensionalVector(new Point(1, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(7, 1), new Point(3, 0))));

}

TEST(TriangleTest, IllegalTest){
    // 兩向量無任一點重疊
    ASSERT_ANY_THROW(Triangle tri1(new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0))));
    // 兩向量為同一向量
    ASSERT_ANY_THROW(Triangle tri2(new TwoDimensionalVector(new Point(3, 4), new Point(1, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(1, 0))));
    // 兩向量平行
    ASSERT_ANY_THROW(Triangle tri3(new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)), new TwoDimensionalVector(new Point(2, 2), new Point(0, 2))));
}

TEST(TriangleTest, AreaTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    ASSERT_NEAR(6, tri.area(), 0.001);
}

TEST(TriangleTest, PerimeterTest){
    // 向量A的x與向量B的x相同
    Triangle tri1(new TwoDimensionalVector(new Point(3, 0), new Point(0, 0)), new TwoDimensionalVector(new Point(3, 0), new Point(3, 4)));
    // 向量A的x與向量B的y相同
    Triangle tri2(new TwoDimensionalVector(new Point(3, 0), new Point(0, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    // 向量A的y與向量B的x相同
    Triangle tri3(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 0), new Point(3, 4)));
    // 向量A的y與向量B的y相同
    Triangle tri4(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    ASSERT_NEAR(12, tri1.perimeter(), 0.001);
    ASSERT_NEAR(12, tri2.perimeter(), 0.001);
    ASSERT_NEAR(12, tri3.perimeter(), 0.001);
    ASSERT_NEAR(12, tri4.perimeter(), 0.001);
}

TEST(TriangleTest, InfoTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    ASSERT_EQ("Triangle (Vector ((0.00, 0.00), (3.00, 0.00)), Vector ((3.00, 4.00), (3.00, 0.00)))", tri.info());
}