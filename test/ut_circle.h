#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/circle.h"

TEST(CircleTest, RadiusTest){
    Circle c(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    ASSERT_NEAR(1, c.radius(), 0.001);
}

TEST(CircleTest, AreaTest){
    Circle c(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    ASSERT_NEAR(3.1415, c.area(), 0.001);
}

TEST(CircleTest, PerimeterTest){
    Circle c(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    ASSERT_NEAR(6.2831, c.perimeter(), 0.001);
}

TEST(CircleTest, InfoTest){
    Circle c(new TwoDimensionalVector(new Point(-4.28, 0.26), new Point(-4.83, 0.73)));
    ASSERT_EQ("Circle (Vector ((-4.28, 0.26), (-4.83, 0.73)))", c.info());
}