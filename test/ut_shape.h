#include <vector>
#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/shape.h"
#include "../src/triangle.h"
#include "../src/rectangle.h"
#include "../src/circle.h"

TEST(ShapeTest, AreaPolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir};
    ASSERT_NEAR(tri.area(), shape_vector[0]->area(), 0.001);
    ASSERT_NEAR(rec.area(), shape_vector[1]->area(), 0.001);
    ASSERT_NEAR(cir.area(), shape_vector[2]->area(), 0.001);        
}

TEST(ShapeTest, PerimeterPolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir};
    ASSERT_NEAR(tri.perimeter(), shape_vector[0]->perimeter(), 0.001);
    ASSERT_NEAR(rec.perimeter(), shape_vector[1]->perimeter(), 0.001);
    ASSERT_NEAR(cir.perimeter(), shape_vector[2]->perimeter(), 0.001);
}

TEST(ShapeTest, InfoPolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir};
    ASSERT_EQ(tri.info(), shape_vector[0]->info());
    ASSERT_EQ(rec.info(), shape_vector[1]->info());
    ASSERT_EQ(cir.info(), shape_vector[2]->info());
}
