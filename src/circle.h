#pragma once

#include <string>
#include <cmath>
#include <sstream>
#include "two_dimensional_vector.h"
#include "shape.h"

class Circle : public Shape
{
private:
    TwoDimensionalVector *_radiusVec;

public:
    Circle(TwoDimensionalVector *radiusVec):_radiusVec(radiusVec) {}
    ~Circle() {}

    double radius() const { return _radiusVec->length(); }

    double area() const override {
        return pow(this->radius(), 2) * M_PI;
    }

    double perimeter() const override {
        return 2 * this->radius() * M_PI;
    }

    std::string info() const override {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << "Circle (" << _radiusVec->info() << ")";
        std::string out = ss.str();
        
        return out;    
    }
};