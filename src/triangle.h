#pragma once

#include <string>
#include <sstream>
#include "shape.h"
#include "two_dimensional_vector.h"

class Triangle : public Shape
{
private:
    TwoDimensionalVector *_v1;
    TwoDimensionalVector *_v2;

public:
    Triangle(TwoDimensionalVector *v1, TwoDimensionalVector *v2): _v1(v1), _v2(v2) {
        bool VectorLegal = *(this->_v1->a()) == *(this->_v2-> a()) || *(this->_v1->a()) == *(this->_v2->b()) || *(this->_v1->b()) == *(this->_v2->a()) || *(this->_v1->b()) == *(this->_v2->b());
        if (VectorLegal == false || _v1->cross(_v2) == 0)
            throw std::invalid_argument("Vector does not conform to specification!");
    }
    ~Triangle() {}

    double area() const override {
        return abs(this->_v1->cross(_v2) / 2);
    }

    double perimeter() const override {
        double side_length_one = this->_v1->length();
        double side_length_two = this->_v2->length();
        double side_length_three;

        if (*(this->_v1->a()) == *(this->_v2->a()))
            side_length_three = sqrt(pow(this->_v1->b()->x() - this->_v2->b()->x(), 2) + pow(this->_v1->b()->y() - this->_v2->b()->y(), 2));
        else if (*(this->_v1->a()) == *(this->_v2->b()))
            side_length_three = sqrt(pow(this->_v1->b()->x() - this->_v2->a()->x(), 2) + pow(this->_v1->b()->y() - this->_v2->a()->y(), 2));
        else if (*(this->_v1->b()) == *(this->_v2->a()))
            side_length_three = sqrt(pow(this->_v1->a()->x() - this->_v2->b()->x(), 2) + pow(this->_v1->a()->y() - this->_v2->b()->y(), 2));
        else if (*(this->_v1->b()) == *(this->_v2->b()))
            side_length_three = sqrt(pow(this->_v1->a()->x() - this->_v2->a()->x(), 2) + pow(this->_v1->a()->y() - this->_v2->a()->y(), 2));
        
        return side_length_one + side_length_two + side_length_three;

    }

    std::string info() const override {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << "Triangle (" << this->_v1->info() << ", " << this->_v2->info() << ")";
        std::string out = ss.str();

        return out; 
    }
};