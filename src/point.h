#pragma once

#include <cmath>
#include <sstream>

class Point
{
private:
    const double _x;
    const double _y;

public:
    Point(double x, double y):_x(x), _y(y) {}
    ~Point() {}

    double x() const { return _x; }

    double y() const { return _y; }

    bool operator==(const Point &pt) const {
        double round_this_x = round(this->x()*100)/100;
        double round_this_y = round(this->y()*100)/100;
        double round_x = round(pt.x()*100)/100;
        double round_y = round(pt.y()*100)/100;

        return round_this_x == round_x && round_this_y == round_y;
    }

    std::string info() const {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << "(" << this->x() << ", " << this->y() << ")";
        std::string out = ss.str();

        return out;
    }
};
